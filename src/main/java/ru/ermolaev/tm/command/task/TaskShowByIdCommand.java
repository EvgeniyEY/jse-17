package ru.ermolaev.tm.command.task;

import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.enumeration.Role;
import ru.ermolaev.tm.model.Task;
import ru.ermolaev.tm.util.TerminalUtil;

public class TaskShowByIdCommand extends AbstractCommand {

    @Override
    public String commandName() {
        return "task-show-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show task by id.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER TASK ID:");
        final String userId = serviceLocator.getAuthenticationService().getUserId();
        final String id = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().findTaskById(userId, id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("[COMPLETE]");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.USER, Role.ADMIN };
    }

}
