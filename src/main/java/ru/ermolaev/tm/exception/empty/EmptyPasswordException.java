package ru.ermolaev.tm.exception.empty;

import ru.ermolaev.tm.exception.AbstractException;

public class EmptyPasswordException extends AbstractException {

    public EmptyPasswordException() {
        super("Error! Password is empty.");
    }

}
