package ru.ermolaev.tm.exception.incorrect;

import ru.ermolaev.tm.exception.AbstractException;

public class IncorrectIndexException extends AbstractException {

    public IncorrectIndexException() {
        super("Error! Index is incorrect.");
    }

    public IncorrectIndexException(final String value) {
        super("Error! This value [" + value + "] is not number.");
    }

}
