package ru.ermolaev.tm.api.repository;

import ru.ermolaev.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    void add(String userId, Task task);

    List<Task> findAll(String userId);

    void remove(String userId, Task task);

    void clear(String userId);

    Task findById(String userId, String id) throws Exception;

    Task findByIndex(String userId, Integer index) throws Exception;

    Task findByName(String userId, String name) throws Exception;

    Task removeById(String userId, String id) throws Exception;

    Task removeByIndex(String userId, Integer index) throws Exception;

    Task removeByName(String userId, String name) throws Exception;

    Task add(Task task);

    void add(List<Task> tasks);

    void add(Task... tasks);

    void clear();

    void load(List<Task> tasks);

    void load(Task... tasks);

    List<Task> getTasksList();

}
