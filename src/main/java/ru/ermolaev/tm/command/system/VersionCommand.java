package ru.ermolaev.tm.command.system;

import ru.ermolaev.tm.command.AbstractCommand;

public final class VersionCommand extends AbstractCommand {

    @Override
    public String commandName() {
        return "version";
    }

    @Override
    public String arg() {
        return "-v";
    }

    @Override
    public String description() {
        return "Show version info.";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.0.15");
    }

}
