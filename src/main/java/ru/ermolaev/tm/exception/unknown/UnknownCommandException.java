package ru.ermolaev.tm.exception.unknown;

import ru.ermolaev.tm.exception.AbstractException;

public class UnknownCommandException extends AbstractException {

    public UnknownCommandException() {
        super("Error! This command does not exist.");
    }

    public UnknownCommandException(final String command) {
        super("Error! This command [" + command + "] does not exist.");
    }

}
