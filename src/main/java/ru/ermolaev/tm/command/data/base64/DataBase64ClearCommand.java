package ru.ermolaev.tm.command.data.base64;

import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.constant.DataConstant;
import ru.ermolaev.tm.enumeration.Role;

import java.io.File;
import java.nio.file.Files;

public class DataBase64ClearCommand extends AbstractCommand {

    @Override
    public String commandName() {
        return "data-base64-clear";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove base64 file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REMOVE BASE64 FILE]");
        final File file = new File(DataConstant.FILE_BASE64);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}
