package ru.ermolaev.tm.command.system;

import ru.ermolaev.tm.command.AbstractCommand;

import java.util.List;

public class CommandsCommand extends AbstractCommand {

    @Override
    public String commandName() {
        return "commands";
    }

    @Override
    public String arg() {
        return "-cmd";
    }

    @Override
    public String description() {
        return "Show application's commands.";
    }

    @Override
    public void execute() {
        final List<AbstractCommand> commands = serviceLocator.getCommandService().getCommandList();
        for (AbstractCommand command: commands) System.out.println(command.commandName());
    }

}
