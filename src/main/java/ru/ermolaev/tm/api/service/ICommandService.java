package ru.ermolaev.tm.api.service;

import ru.ermolaev.tm.command.AbstractCommand;

import java.util.List;

public interface ICommandService {

    List<AbstractCommand> getCommandList();

}
