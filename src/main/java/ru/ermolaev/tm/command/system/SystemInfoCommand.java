package ru.ermolaev.tm.command.system;

import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.util.NumberUtil;

public final class SystemInfoCommand extends AbstractCommand {

    @Override
    public String commandName() {
        return "info";
    }

    @Override
    public String arg() {
        return "-i";
    }

    @Override
    public String description() {
        return "Show hardware info.";
    }

    @Override
    public void execute() {
        System.out.println("Available processors (cores): " + Runtime.getRuntime().availableProcessors());
        System.out.println("Free memory: " + NumberUtil.formatBytes(Runtime.getRuntime().freeMemory()));
        System.out.println("Maximum memory: " + (Runtime.getRuntime().maxMemory() == Long.MAX_VALUE ? "no limit" : NumberUtil.formatBytes(Runtime.getRuntime().maxMemory())));
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(Runtime.getRuntime().totalMemory()));
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory()));
    }

}
