package ru.ermolaev.tm.command.data.base64;

import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.constant.DataConstant;
import ru.ermolaev.tm.dto.Domain;
import ru.ermolaev.tm.enumeration.Role;
import sun.misc.BASE64Decoder;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

public class DataBase64LoadCommand extends AbstractCommand {

    @Override
    public String commandName() {
        return "data-base64-load";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Load data from base64 file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BASE64 LOAD]");
        final String base64date = new String(Files.readAllBytes(Paths.get(DataConstant.FILE_BASE64)));
        final byte[] decodedData = new BASE64Decoder().decodeBuffer(base64date);
        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(decodedData);
        final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        final Domain domain = (Domain) objectInputStream.readObject();

        serviceLocator.getTaskService().load(domain.getTasks());
        serviceLocator.getProjectService().load(domain.getProjects());
        serviceLocator.getUserService().load(domain.getUsers());

        objectInputStream.close();
        byteArrayInputStream.close();
        System.out.println("[OK]");

    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}
