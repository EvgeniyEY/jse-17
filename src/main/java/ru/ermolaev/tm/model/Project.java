package ru.ermolaev.tm.model;

import java.io.Serializable;

public class Project extends AbstractModel implements Serializable {

    private static final long serialVersionUID = 1001L;

    private String name = "";

    private String description = "";

    private String userId;

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(final String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "Project [" +
                "Name='" + name + '\'' +
                ", Description='" + description + '\'' +
                ']';
    }

}
