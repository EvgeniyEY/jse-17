package ru.ermolaev.tm.repository;

import ru.ermolaev.tm.api.repository.ICommandRepository;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.command.data.base64.DataBase64ClearCommand;
import ru.ermolaev.tm.command.data.base64.DataBase64LoadCommand;
import ru.ermolaev.tm.command.data.base64.DataBase64SaveCommand;
import ru.ermolaev.tm.command.data.binary.DataBinaryClearCommand;
import ru.ermolaev.tm.command.data.binary.DataBinaryLoadCommand;
import ru.ermolaev.tm.command.data.binary.DataBinarySaveCommand;
import ru.ermolaev.tm.command.user.*;
import ru.ermolaev.tm.command.project.ProjectsClearCommand;
import ru.ermolaev.tm.command.project.ProjectCreateCommand;
import ru.ermolaev.tm.command.project.ProjectRemoveByIdCommand;
import ru.ermolaev.tm.command.project.ProjectRemoveByIndexCommand;
import ru.ermolaev.tm.command.project.ProjectRemoveByNameCommand;
import ru.ermolaev.tm.command.project.ProjectShowByIdCommand;
import ru.ermolaev.tm.command.project.ProjectShowByIndexCommand;
import ru.ermolaev.tm.command.project.ProjectShowByNameCommand;
import ru.ermolaev.tm.command.project.ProjectListShowCommand;
import ru.ermolaev.tm.command.project.ProjectUpdateByIdCommand;
import ru.ermolaev.tm.command.project.ProjectUpdateByIndexCommand;
import ru.ermolaev.tm.command.system.*;
import ru.ermolaev.tm.command.task.TasksClearCommand;
import ru.ermolaev.tm.command.task.TaskCreateCommand;
import ru.ermolaev.tm.command.task.TaskRemoveByIdCommand;
import ru.ermolaev.tm.command.task.TaskRemoveByIndexCommand;
import ru.ermolaev.tm.command.task.TaskRemoveByNameCommand;
import ru.ermolaev.tm.command.task.TaskShowByIdCommand;
import ru.ermolaev.tm.command.task.TaskShowByIndexCommand;
import ru.ermolaev.tm.command.task.TaskShowByNameCommand;
import ru.ermolaev.tm.command.task.TaskListShowCommand;
import ru.ermolaev.tm.command.task.TaskUpdateByIdCommand;
import ru.ermolaev.tm.command.task.TaskUpdateByIndexCommand;

import java.util.ArrayList;
import java.util.List;

public class CommandRepository implements ICommandRepository {

    private final List<AbstractCommand> commandList = new ArrayList<>();

    {
        commandList.add(new HelpCommand());
        commandList.add(new AboutCommand());
        commandList.add(new SystemInfoCommand());
        commandList.add(new VersionCommand());
        commandList.add(new CommandsCommand());
        commandList.add(new ArgumentsCommand());
        commandList.add(new ExitCommand());

        commandList.add(new UserLoginCommand());
        commandList.add(new UserLogoutCommand());
        commandList.add(new UserRegistrationCommand());
        commandList.add(new UserProfileShowCommand());
        commandList.add(new UserEmailUpdateCommand());
        commandList.add(new UserFirstNameUpdateCommand());
        commandList.add(new UserMiddleNameUpdateCommand());
        commandList.add(new UserLastNameUpdateCommand());
        commandList.add(new UserPasswordUpdateCommand());
        commandList.add(new UserLockCommand());
        commandList.add(new UserUnlockCommand());
        commandList.add(new UserRemoveCommand());

        commandList.add(new TaskCreateCommand());
        commandList.add(new TasksClearCommand());
        commandList.add(new TaskListShowCommand());
        commandList.add(new TaskShowByIdCommand());
        commandList.add(new TaskShowByIndexCommand());
        commandList.add(new TaskShowByNameCommand());
        commandList.add(new TaskRemoveByIdCommand());
        commandList.add(new TaskRemoveByIndexCommand());
        commandList.add(new TaskRemoveByNameCommand());
        commandList.add(new TaskUpdateByIdCommand());
        commandList.add(new TaskUpdateByIndexCommand());

        commandList.add(new ProjectCreateCommand());
        commandList.add(new ProjectsClearCommand());
        commandList.add(new ProjectListShowCommand());
        commandList.add(new ProjectShowByIdCommand());
        commandList.add(new ProjectShowByIndexCommand());
        commandList.add(new ProjectShowByNameCommand());
        commandList.add(new ProjectRemoveByIdCommand());
        commandList.add(new ProjectRemoveByIndexCommand());
        commandList.add(new ProjectRemoveByNameCommand());
        commandList.add(new ProjectUpdateByIdCommand());
        commandList.add(new ProjectUpdateByIndexCommand());

        commandList.add(new DataBinaryLoadCommand());
        commandList.add(new DataBinarySaveCommand());
        commandList.add(new DataBinaryClearCommand());
        commandList.add(new DataBase64LoadCommand());
        commandList.add(new DataBase64SaveCommand());
        commandList.add(new DataBase64ClearCommand());
    }

    @Override
    public List<AbstractCommand> getCommandList() {
        return commandList;
    }

}
