package ru.ermolaev.tm.api.repository;

import ru.ermolaev.tm.command.AbstractCommand;

import java.util.List;

public interface ICommandRepository {

    List<AbstractCommand> getCommandList();

}
