package ru.ermolaev.tm.command.task;

import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.enumeration.Role;
import ru.ermolaev.tm.model.Task;
import ru.ermolaev.tm.util.TerminalUtil;

public class TaskShowByIndexCommand extends AbstractCommand {

    @Override
    public String commandName() {
        return "task-show-by-index";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show task by index.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER TASK INDEX:");
        final String userId = serviceLocator.getAuthenticationService().getUserId();
        final Integer index = TerminalUtil.nextNumber() -1;
        final Task task = serviceLocator.getTaskService().findTaskByIndex(userId, index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("[COMPLETE]");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.USER, Role.ADMIN };
    }

}
