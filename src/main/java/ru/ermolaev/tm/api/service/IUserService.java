package ru.ermolaev.tm.api.service;

import ru.ermolaev.tm.model.User;
import ru.ermolaev.tm.enumeration.Role;

import java.util.List;

public interface IUserService {

    List<User> findAll();

    User create(String login, String password) throws Exception;

    User create(String login, String password, String email) throws Exception;

    User create(String login, String password, Role role) throws Exception;

    User updatePassword(String userId, String newPassword) throws Exception;

    User findById(String id) throws Exception;

    User findByLogin(String login) throws Exception;

    User findByEmail(String email) throws Exception;

    User removeUser(User user);

    User removeById(String id) throws Exception;

    User removeByLogin(String login) throws Exception;

    User removeByEmail(String email) throws Exception;

    User updateUserFirstName(String userId, String newFirstName) throws Exception;

    User updateUserMiddleName(String userId, String newMiddleName) throws Exception;

    User updateUserLastName(String userId, String newLastName) throws Exception;

    User updateUserEmail(String userId, String newEmail) throws Exception;

    User lockUserByLogin(String login) throws Exception;

    User unlockUserByLogin(String login) throws Exception;

    void load(List<User> users);

    void load(User... users);

}
