package ru.ermolaev.tm.service;

import ru.ermolaev.tm.api.repository.ICommandRepository;
import ru.ermolaev.tm.api.service.ICommandService;
import ru.ermolaev.tm.command.AbstractCommand;

import java.util.List;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public List<AbstractCommand> getCommandList() {
        return commandRepository.getCommandList();
    }

}
