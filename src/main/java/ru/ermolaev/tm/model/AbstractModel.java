package ru.ermolaev.tm.model;

import java.io.Serializable;
import java.util.UUID;

public abstract class AbstractModel implements Serializable {

    private static final long serialVersionUID = 1001L;

    private String id = UUID.randomUUID().toString();

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

}
