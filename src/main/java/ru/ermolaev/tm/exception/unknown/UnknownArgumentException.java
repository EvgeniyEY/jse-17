package ru.ermolaev.tm.exception.unknown;

import ru.ermolaev.tm.exception.AbstractException;

public class UnknownArgumentException extends AbstractException {

    public UnknownArgumentException() {
        super("Error! This argument does not exist.");
    }

    public UnknownArgumentException(final String argument) {
        super("Error! This argument [" + argument + "] does not exist.");
    }

}
