package ru.ermolaev.tm.command.system;

import ru.ermolaev.tm.command.AbstractCommand;

public final class ExitCommand extends AbstractCommand {

    @Override
    public String commandName() {
        return "exit";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Exit from application.";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
