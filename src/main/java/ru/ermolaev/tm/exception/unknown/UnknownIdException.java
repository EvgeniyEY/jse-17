package ru.ermolaev.tm.exception.unknown;

import ru.ermolaev.tm.exception.AbstractException;

public class UnknownIdException extends AbstractException {

    public UnknownIdException() {
        super("Error! This ID does not exist.");
    }

    public UnknownIdException(final String id) {
        super("Error! This ID [" + id + "] does not exist.");
    }

}
