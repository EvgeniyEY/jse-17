package ru.ermolaev.tm.api.service;

import ru.ermolaev.tm.model.Project;

import java.util.List;

public interface IProjectService {

    void createProject(String userId, String name) throws Exception;

    void createProject(String userId, String name, String description) throws Exception;

    void addProject(String userId, Project project) throws Exception;

    List<Project> showAllProjects(String userId) throws Exception;

    void removeProject(String userId, Project project) throws Exception;

    void removeAllProjects(String userId) throws Exception;

    Project findProjectById(String userId, String id) throws Exception;

    Project findProjectByIndex(String userId, Integer index) throws Exception;

    Project findProjectByName(String userId, String name) throws Exception;

    Project updateProjectById(String userId, String id, String name, String description) throws Exception;

    Project updateProjectByIndex(String userId, Integer index, String name, String description) throws Exception;

    Project removeProjectById(String userId, String id) throws Exception;

    Project removeProjectByIndex(String userId, Integer index) throws Exception;

    Project removeProjectByName(String userId, String name) throws Exception;

    void load(List<Project> projects);

    void load(Project... projects);

    List<Project> getProjectsList();

}
