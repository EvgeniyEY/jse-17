package ru.ermolaev.tm.constant;

public class DataConstant {

    public static final String FILE_BINARY = "./data.bin";

    public static final String FILE_BASE64 = "./data.base64";

}
