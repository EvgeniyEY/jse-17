package ru.ermolaev.tm.command.user;

import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.enumeration.Role;
import ru.ermolaev.tm.util.TerminalUtil;

public class UserEmailUpdateCommand extends AbstractCommand {

    @Override
    public String commandName() {
        return "user-update-email";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update user e-mail.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATE USER EMAIL]");
        System.out.println("ENTER NEW USER EMAIL:");
        final String newEmail = TerminalUtil.nextLine();
        serviceLocator.getAuthenticationService().updateUserEmail(newEmail);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.USER, Role.ADMIN };
    }

}
