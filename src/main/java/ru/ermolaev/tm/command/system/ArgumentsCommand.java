package ru.ermolaev.tm.command.system;

import ru.ermolaev.tm.command.AbstractCommand;

import java.util.List;

public class ArgumentsCommand extends AbstractCommand {

    @Override
    public String commandName() {
        return "arguments";
    }

    @Override
    public String arg() {
        return "-arg";
    }

    @Override
    public String description() {
        return "Show application's arguments.";
    }

    @Override
    public void execute() {
        final List<AbstractCommand> commands = serviceLocator.getCommandService().getCommandList();
        for (AbstractCommand command: commands) {
            if (command.arg() == null) continue;
            System.out.println(command.arg());
        }
    }

}
