package ru.ermolaev.tm.exception.empty;

import ru.ermolaev.tm.exception.AbstractException;

public class EmptyNameException extends AbstractException {

    public EmptyNameException() {
        super("Error! Name is empty.");
    }

}
