package ru.ermolaev.tm.command.task;

import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.enumeration.Role;
import ru.ermolaev.tm.util.TerminalUtil;

public class TaskCreateCommand extends AbstractCommand {

    @Override
    public String commandName() {
        return "task-create";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Create a new task.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CREATE TASK]");
        final String userId = serviceLocator.getAuthenticationService().getUserId();
        System.out.println("ENTER TASK NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER TASK DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        serviceLocator.getTaskService().createTask(userId, name, description);
        System.out.println("[COMPLETE]");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.USER, Role.ADMIN };
    }

}
