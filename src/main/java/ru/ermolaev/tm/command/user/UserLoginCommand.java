package ru.ermolaev.tm.command.user;

import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.exception.user.AlreadyLoggedInException;
import ru.ermolaev.tm.util.TerminalUtil;

public class UserLoginCommand extends AbstractCommand {

    @Override
    public String commandName() {
        return "login";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Login in your account.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOGIN]");
        if (serviceLocator.getAuthenticationService().isAuthenticated()) throw new AlreadyLoggedInException();
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthenticationService().login(login, password);
        System.out.println("[OK]");
    }

}
