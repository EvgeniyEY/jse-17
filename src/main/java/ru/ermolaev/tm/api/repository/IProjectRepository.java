package ru.ermolaev.tm.api.repository;

import ru.ermolaev.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    void add(String userId, Project project);

    List<Project> findAll(String userId);

    void remove(String userId, Project project);

    void clear(String userId);

    Project findById(String userId, String id) throws Exception;

    Project findByIndex(String userId, Integer index) throws Exception;

    Project findByName(String userId, String name) throws Exception;

    Project removeById(String userId, String id) throws Exception;

    Project removeByIndex(String userId, Integer index) throws Exception;

    Project removeByName(String userId, String name) throws Exception;

    Project add(Project project);

    void add(List<Project> projects);

    void add(Project... projects);

    void clear();

    void load(List<Project> projects);

    void load(Project... projects);

    List<Project> getProjectsList();

}
