package ru.ermolaev.tm.exception.empty;

import ru.ermolaev.tm.exception.AbstractException;

public class EmptyCommandException extends AbstractException {

    public EmptyCommandException() {
        super("Error! Command is empty.");
    }

}
